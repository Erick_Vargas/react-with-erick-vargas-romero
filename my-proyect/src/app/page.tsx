import Image from "../../node_modules/next/image";
import next from "next";
import styles from "./page.module.css";
import { Header } from "@/component/Header";
import Button from "@/component/Button";
import Empresas from "@/component/Empresas";
import Title from "@/component/Title";
import Informacion from "@/component/Informacion";
import { Footer } from "@/component/Footer";
import { Box } from "@mui/material";
import Cards from "@/component/Card";

export default function Home() {
  return (
    <main>
      <Header/>
      <section className="container hero">
          <div className="bg-text">
            <h1>Buy, trade, and hold 350+ cryptocurrencies</h1>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat nulla suspendisse tortor aenean dis placerat.</h4>
            <div>
              <Button title="Download App" primary/>
              <Button title="view pricing" transparent/>
            </div>
          </div>
          <div>
            <Image src="/images/computers.svg" alt="" height="100" width="100"></Image>
          </div>
      </section>
      <section className="Empresas container">
        <Empresas imageUrl="/images/empresa1.svg"/>
        <Empresas imageUrl="/images/empresa2.svg"/>
        <Empresas imageUrl="/images/empresa3.svg"/>
        <Empresas imageUrl="/images/empresa4.svg"/>
        <Empresas imageUrl="/images/empresa5.svg"/>
      </section>
      <Title title="Build your crypto portfolio" subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat nulla suspendisse tortor aene."></Title>
      <Informacion title="Earn daily rewards on your idle tokens" imageUrl="/images/phones.svg" subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat nulla suspendisse tortor aene." />
      <Informacion title="Earn daily rewards on your idle tokens" imageUrl="/images/computers.svg" subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat nulla suspendisse tortor aene." reverce/>
      <Box sx={{display:"flex",width:"100%"}}>
        <Footer/>
      </Box>
    </main>
  );
}
