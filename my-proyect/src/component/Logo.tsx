export default function Logo(){
    return(
        <div className="logo-principal">
            <i className="icon icon-logo"></i>
            <span>FinanceFlow</span>
        </div>
    )
}
