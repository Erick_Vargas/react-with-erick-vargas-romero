import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Logo from './Logo';
import Cards from './Card';

const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

const card = (
  <React.Fragment >
    <Box sx={{display:"flex"}}>
        <Box>
        <Logo></Logo>
    <Typography sx={{borderBottom:"1px solid white",marginTop:"150px"}} variant="h4" gutterBottom>
        Menu
      </Typography>
      <Box sx={{display:"flex"}}>
        <CardContent>
            <Typography variant="body1" gutterBottom>
            Home
            </Typography>
            <Typography variant="body1" gutterBottom>
            About
            </Typography>
            <Typography variant="body1" gutterBottom>
            Pricing
            </Typography>
        </CardContent>
        <CardContent sx={{marginLeft:"80px"}}>
            <Typography variant="body1" gutterBottom>
            Tokens
            </Typography>
            <Typography variant="body1" gutterBottom>
            Blog
            </Typography>
            <Typography variant="body1" gutterBottom>
            contact us
            </Typography>
        </CardContent>
      </Box>
    </Box>
    <Box sx={{display:"flex", marginLeft:"60%"}}>
      <Cards></Cards>
    </Box>
    </Box>
    
  </React.Fragment>
);

export default function MyCard() {
  return (
    <Box sx={{ minWidth: 275 }}>
      <Card sx={{background:"transparent", border:"none"}} variant="outlined">{card}</Card>
    </Box>
  );
}
