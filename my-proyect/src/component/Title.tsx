import Image from "../../node_modules/next/image";

interface PropsTitle{
    title:string,
    subtitle:string,
    info?:boolean
}
export default function Title({title,subtitle,info}:PropsTitle){
    return(
        info?
        <div>
            <h1>{title}</h1>
            <h5>{subtitle}</h5>
        </div>
        :
        <div className="titulo">
            <h1>{title}</h1>
            <h5>{subtitle}</h5>
        </div>
    )
}