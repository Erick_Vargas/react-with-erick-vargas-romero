interface PropsButton{
    title?:string,
    primary?:boolean,
    transparent?:boolean
}

export default function Button(props:PropsButton){
    return(
        props.transparent?
        <button className="btn btn-transparent">{props.title}</button>
        :
        <button className="btn btn-primary">{props.title}</button>
    )
}