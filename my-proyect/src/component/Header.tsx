import Button from './Button'
import Logo from './Logo'
import Link from '../../node_modules/next/link'

export function Header(){
    return (
        <section className="container nav-header">
            <Logo/>
            <ul>
                <li>
                    <Link href="/" className='active'>Home</Link>
                </li>
                <li>
                    <Link href="/discover">About</Link>
                </li>
                <li>
                    <Link href="/special-deals">Pricing</Link>
                </li>
                <li>
                    <Link href="/contact">Tokens</Link>
                </li>
                <li>
                    <Link href="/contact">Blog</Link>
                </li>
                <li>
                    <Link href="/contact">contact us</Link>
                </li>
            </ul>
            <div>
                <Button title="Download App" primary/>
            </div>
        </section>
    )
}
