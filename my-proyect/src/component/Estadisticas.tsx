import Image from "../../node_modules/next/image";

interface PropsEstadisticas{
    imgUrl:any,
    param:string
}

export default function Estadistica({imgUrl,param}:PropsEstadisticas){
    return(
        <div className="status">
            <i><Image src={imgUrl} alt="" width={60} height={60}/></i>
            <p>{param}</p>
        </div>
    )
}