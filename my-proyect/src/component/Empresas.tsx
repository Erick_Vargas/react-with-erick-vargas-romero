import Image from "../../node_modules/next/image";

interface PropsEmpresas{
    imageUrl:any
}

export default function Empresas({imageUrl}:PropsEmpresas){
    return(
        <Image src={imageUrl} alt="" width={100} height={100}/>
    )
}