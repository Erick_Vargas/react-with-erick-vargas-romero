import Image from "../../node_modules/next/image";
import Estadistica from "./Estadisticas";
import Title from "./Title";

interface PropsInformacion{
    imageUrl:any,
    title:string,
    subtitle:string,
    reverce?:boolean
}

export default function Informacion({imageUrl,title,subtitle,reverce}:PropsInformacion){
    return(
        reverce?
        <section className="container informacion">
            <div>
                <Title title={title} subtitle={subtitle} info/>
                <Estadistica imgUrl="/images/bg4.svg" param="100% Private data"></Estadistica>
                <Estadistica imgUrl="/images/bg5.svg" param="99.99% Uptime guarantee"></Estadistica>
                <Estadistica imgUrl="/images/bg6.svg" param="24/7 Dedicated support"></Estadistica>
            </div>
            <Image  className="reverse" src={imageUrl} alt="" width={100} height={100}/>
        </section>
        :
        <section className="container informacion">
            <Image src={imageUrl} alt="" width={100} height={100}/>
            <div>
                <Title title={title} subtitle={subtitle} info/>
                <Estadistica imgUrl="/images/bg1.svg" param="Lowest fees in market"></Estadistica>
                <Estadistica imgUrl="/images/bg2.svg" param="Fast and secure transactions"></Estadistica>
                <Estadistica imgUrl="/images/bg3.svg" param="256-bit secure encryption"></Estadistica>
            </div>
        </section>
    )
}