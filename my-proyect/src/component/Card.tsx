import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Box, Fab } from '@mui/material';
import AppleIcon from '@mui/icons-material/Apple';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

export default function Cards() {
  return (
    <Card sx={{ width: 400, height: 400,background:"transparent"}}>
        <Box sx={{display:"flex",justifyContent:"end"}}>
            <InstagramIcon sx={{backgroundColor:"#fff", marginLeft:"5px", width:"40px", height:"40px"}}/>
            <FacebookIcon sx={{backgroundColor:"#fff", marginLeft:"5px", width:"40px", height:"40px"}}/>
            <LinkedInIcon sx={{backgroundColor:"#fff", marginLeft:"5px", width:"40px", height:"40px"}}/>
        </Box>
        <Box sx={{background:"#272680",borderRadius:"50px",width:"100%",height:"100%",padding:"80px 10px"}}>
            <CardContent>
        <Typography gutterBottom variant="h5" component="div">
        Download our Application
        </Typography>
        <Typography variant="body2" color="text.secondary">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed nulla integer 
        </Typography>
      </CardContent>
      <CardActions>
        <Fab variant="extended" color="primary" sx={{background:"blue"}}>
            <AppleIcon sx={{ mr: 1 }} />
            App Store
        </Fab>
        <Fab variant="extended" color="primary" sx={{background:"blue"}}>
            <PlayArrowIcon sx={{ mr: 1 }} />
            PLAY STORE
        </Fab>
      </CardActions>
        </Box>
      
    </Card>
  );
}
